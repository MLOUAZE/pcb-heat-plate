Length=150*10^-3;
Width=120*10^-3;
Thickness=1.6*10^-3;
FR4_Density=1850; %kg/m³
Mass=Length*Width*Thickness*FR4_Density;
FR4_Specific_heat=1.15*10^3; %specific heat in W.s/Kg/°C

h=10;

C=FR4_Specific_heat*Mass; %W.s/°C
R=2*h*Width*Length; %W/°C

tau=R*C;  %s

alphamax=1;
deltaTmax=195;
G=deltaTmax/alphamax;

Hp=tf([G], [tau 1]);

T0=tau/1.2;
Kp=tau/(G*T0);
Ti=tau;

K=tf([Kp*Ti Kp], [Ti 0]);
fs=5;
Kz=c2d(K,1/fs);
Kz.Variable=('z^-1'); Kz
[num den]=tfdata(Kz,'v');
