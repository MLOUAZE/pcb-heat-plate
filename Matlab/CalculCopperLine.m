clear all;
%%thermal and power supply parameters
T_ext=20;
T_PCB=180;
h=10;
U=12;

%%Copper line parameters
Thickness_copper=17.5*10^-6;
Rho_copper=1.72*10^-8;
Alpha_copper=3.8*10^-3;
N=12;

%%PCB size paramaters
Length=0.15;
Width=0.12;
Gap_X=0.005;
Gap_Y_Down=0.002;
Gap_Y_Up=0.005;

%%PCB connector size parameters
Size_conn_X=0.01;
Size_conn_Y=0.01;
Gap_copper_conn=0.002;

%%%DO NOT MODIFY THIS PART
%%Calculation K constant
W=h*2*Length*Width*(T_PCB-T_ext);
K=(U^2*Thickness_copper)/(Rho_copper*(1+Alpha_copper*(T_PCB-T_ext))*W);

alpha=Length-Gap_Y_Down-Gap_Y_Up-Size_conn_Y-Gap_copper_conn;
beta=Width-2*Gap_X;
w=(2*N*alpha+beta)./(K+2*N);
L=2*N.*(alpha-w)+beta;
b=(beta-2*N.*w)./(2*N-1);

%%Plot of the border of the PCB
Plate_X=[0 0 Width Width]; Plate_Y=[0 Length 0 Length];
plot(Plate_X,Plate_Y,'o');
hold on;
axis equal

%%Plot of the connector
Anode_Conn_X=[Gap_X Gap_X Gap_X+Size_conn_X Gap_X+Size_conn_X];
Anode_Conn_Y=[Gap_Y_Down Gap_Y_Down+Size_conn_Y Gap_Y_Down+Size_conn_Y Gap_Y_Down];
Cathode_Conn_X=Anode_Conn_X+Width-2*Gap_X- Size_conn_X;
Cathode_Conn_Y=Anode_Conn_Y;
plot(Anode_Conn_X,Anode_Conn_Y,'x'); plot(Cathode_Conn_X,Cathode_Conn_Y,'x');

%plot of the track
A=repmat([Length-Gap_Y_Up Length-Gap_Y_Up Gap_Y_Down+Size_conn_Y+Gap_copper_conn+w Gap_Y_Down+Size_conn_Y+Gap_copper_conn+w],1, N-1);
B=[Length-Gap_Y_Up Length-Gap_Y_Up Gap_Y_Down+Size_conn_Y Gap_Y_Down+Size_conn_Y];
C=repmat([Length-Gap_Y_Up-w Length-Gap_Y_Up-w Gap_Y_Down+Size_conn_Y+Gap_copper_conn Gap_Y_Down+Size_conn_Y+Gap_copper_conn],1, N-1);
D=[Length-Gap_Y_Up-w Length-Gap_Y_Up-w Gap_Y_Down+Size_conn_Y Gap_Y_Down+Size_conn_Y];
point_Y=[A B C D];

A=repmat([Gap_X Gap_X+2*w+b Gap_X+2*w+b Gap_X+2*w+2*b],1,N-1)+sort(repmat((2*b+2*w).*(0:(N-2)),1,4));
B=[Width-Gap_X-2*w-b Width-Gap_X Width-Gap_X Width-Gap_X-w];
C=repmat([Gap_X+w+b Gap_X+3*w+2*b Gap_X+3*w+2*b Gap_X+3*w+3*b],1,N-1)+sort(repmat((2*b+2*w).*(0:(N-2)),1,4));
C=C(end:-1:1);
D=[Gap_X+b+w Gap_X+w Gap_X+w Gap_X];

point_X=[A B C D];

plot(point_X,point_Y,'-*');

