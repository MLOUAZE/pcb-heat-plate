T0_C=25; T0_K=T0_C+273.15;
R0=100*10^3; 
B=3955;
RPol=5*10^3;
Vin=5;
Vmeas=(1:Vin*100)/100;
T_K=(T0_K*B)./(log(RPol./(R0*(Vin./Vmeas-1)))*T0_K+B);
T_C=T_K-273.15;

index=T_C>20&T_C<180;
Vmeas=Vmeas(index);
T_C=T_C(index);

plot(Vmeas,T_C);
hold on;
p=polyfit(Vmeas,T_C,3);
T_C_Approx=polyval(p,Vmeas);
plot(Vmeas, T_C_Approx);
avg_rms_error=sqrt(sum((T_C_Approx-T_C).^2))/size(Vmeas,2);
max_rms_error=sqrt(max((T_C_Approx-T_C).^2));