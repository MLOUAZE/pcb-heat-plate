### PCB Heat plate Project by Mathis LOUAZÉ

This repository contains all the files made for the PCB Heat plate project. This project was made for the course _"Design of electronic devices and systems"_ in fall 2022 at Aalto University.

The repository contains 5 folders :
- Comsol : contains the files used for the thermal simulation
- FreeCad : contains the 3D files of the device
- KiCad : Contains the files of the PCB circuits
- Matlab : Contains the files used for calculation of different parameters of the system

This repository also contains the .pdf file detailing all the work done.

##Comsol
The folder contains 3 files : 
- heating_circuit_polygon.txt, this file contains the position for the polygon used to model the copper trace
- heat_circuit.mph, this file is the one used in Comsol for the simulation

##FreeCad
This folder contains only 1 file "Complete_assembly.FCstd", this single file contains all the models of the device

##Kicad
This folder has 2 subfolders, one for the electronic PCB the other one for the heat plate PCB.

##Matlab
This folder contains 3 .m files :
- CalculRsensor.m, use to calculate all the parameters regarding the temperature measurement using the IR thermal sensor
- CalculCopperLine.m, use to calculate all the parameters for the copper trace on the heating plate
- CalculPIController.m, use to calculate the PI controller for the temperature of the heat plate

The folder also contains simulations file for 2 simulations :
- Thermal_model_open_loop, contains the open loop simulation
- Thermal_model, contains the closed loop simulation
NB: those file were made using matlab 2022a version 
